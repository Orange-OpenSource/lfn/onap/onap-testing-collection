# Run ONAP test

## Purpose

These roles are usable from the `run_onap_test` playbook.

## Requirements

- **A standalone server** with access to ONAP System Under Test

- **The kubernetes credentials** (~/.kube/config) on the server

- **The Openstack test tenant credentials** (~/.config/openstack/clouds.yaml) on
  the server

- **Docker engine** installed

## Parameters

### General parameters

<!-- markdown-link-check-disable -->
<!-- markdownlint-disable line-length -->
| Variable                                | Purpose                                     | Default value                                      |
|-----------------------------------------|---------------------------------------------|----------------------------------------------------|
| onap_testing_test_name                  | Nameof the test                             | Not set                                            |
| onap_testing_test_category              | Category of tests to be run                 | Not set                                            |
| onap_testing_nb_max_retry               | Number of retries in case of failure        | 1                                                  |
| onap_testing_run_timeout                | Test timeout (in minutes)                   | 60                                                 |
|-----------------------------------------|---------------------------------------------|----------------------------------------------------|
| kube_conf_file                          | Name of the Kubernetes config file          | config                                             |
| kube_conf_path                          | Path of the Kubernetes config file          | .kube/<kube_conf_file>                             |
| onap_testing_docker_registry            | ONAP xtesting docker registry               | nexus3.onap.org:10003                              |
| onap_testing_docker_path                | ONAP xtesting docker full path              | "{{ onap_testing_docker_registry }}/onap"          |
| onap_namespace                          | ONAP namespace                              | onap                                               |
| onap_version                            | ONAP version to be tested                   | master                                             |
| onap_testing_docker_version             | xtesting docker version                     | same as onap_version                               |
| onap_install_external_requirements      | Allow the install the external requirements | true                                               |
|-----------------------------------------|---------------------------------------------|----------------------------------------------------|
| s3_endpoint                             | S3 end point to store artifacts             | '' or env variable S3_ENDPOINT_URL                 |
| s3_internal_url                         | S3 internal url                             | '' or env variable S3_INTERNAL_ENDPOINT_URL        |
| s3_http_url_endpoint                    | S3 HTTP url                                 | '' or env variable S3_ENDPOINT_URL                 |
| s3_access_key                           | S3 access key                               | '' or env variable S3_ACCESS_KEY                   |
| s3_secret_key                           | S3 secret key                               | '' or env variable S3_SECRET_KEY                   |
|-----------------------------------------|---------------------------------------------|----------------------------------------------------|
| test_result_url                         | DB URL storing the xteeting results         | <http://testresults.opnfv.org/onap/api/v1/results> |
|                                         |                                             | or env variable TEST_RESULT_DB_URL                 |
| deployment_name                         | Deployment name (xtesting result param)     | oom or env variable DEPLOYMENT                     |
| node_name                               | Lab where the test has been executed        | lab-test or env variable POD                       |
| scenario                                | Test scenario (xtesting result param)       | scenario-test of env variable DEPLOY_SCENARIO      |
| build_tag                               | Build tag (xtesting result param)           | build-tag-test or env variable BUILD_TAG           |
|-----------------------------------------|---------------------------------------------|----------------------------------------------------|
<!-- markdownlint-enable line-length -->
<!-- markdown-link-check-enable -->

onap_install_external_requirements needs to be set to false when the role run_onap_test is run from a gitlab runner.
In that case, the requirements are set in the used docker container.

### Parameters for the docker based tests

<!-- markdownlint-disable line-length -->
| Variable                                | Purpose                                     | Default value                                      |
|-----------------------------------------|---------------------------------------------|----------------------------------------------------|
| os_test_cloud                           | The target cloud tenant for VNF tests       | cloud-test or env variable CNF_TEST_K8S_NAMESPACE  |
| clouds_path                             | The path of the openstack clouds.yaml       | .config/openstacl/clouds.yaml                      |
| onap_testing_cnf_region_type            | Region type for CNF tests                   | k8s or env variable CNF_TEST_K8S_REGION_TYPE       |
| onap_testing_cnf_k8s_config             | Cluster config for CNF tests                |  None or env variable CNF_TEST_K8S_CONFIG          |
| onap_testing_cnf_k8s_namespace          | Cluster testing namespace for CNF tests     | None or env variable CNF_TEST_K8S_NAMESPACE        |
|-----------------------------------------|---------------------------------------------|----------------------------------------------------|
| onap_testing_local_path                 | Path on testing VM to get results           | ~/xtesting-test                                    |
|-----------------------------------------|---------------------------------------------|----------------------------------------------------|
| oom_cluster_ip                          | IP address of Kubernetes first node         | Not set (will search in /etc/host if not set)      |
|-----------------------------------------|---------------------------------------------|----------------------------------------------------|
<!-- markdownlint-enable line-length -->

### Parameters for the kubernetes job based tests

<!-- markdownlint-disable line-length -->
| Variable                                | Purpose                                     | Default value                                      |
|-----------------------------------------|---------------------------------------------|----------------------------------------------------|
| onap_testing_robot_configmap            | Name of the ONAP robot configmap            | robot-eteshare-configmap                           |
|-----------------------------------------|---------------------------------------------|----------------------------------------------------|
<!-- markdownlint-enable line-length -->

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.onap_testing.run_onap_test
```
