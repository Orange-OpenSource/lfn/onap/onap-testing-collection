# ONAP testing collection

The goal of this community collection consists in executing ONAP
community tests. Details are given in the playbook documentation.

It consists mainly in one single [playbook](./playbooks/run_onap_test.md)
leveraging an associated role.

## Playbooks

| Playbook                            | Description                  |
| ----------------------------------- | ---------------------------- |
| [run](./playbooks/run_onap_test.md) | Execute ONAP community tests |

## Roles
<!-- markdownlint-disable line-length -->
| Role                            | Description                                     |
| ------------------------------- | ----------------------------------------------- |
| [run_onap](./roles/run_onap.md) | Role associated with the run_onap_test playbook |
<!-- markdownlint-enable line-length -->

## Versions

| ORONAP version | Collection sha1                          |
| -------------- | ---------------------------------------- |
| Istanbul       | b35cb1e4564dffbb2e8034982e410b43e9ec5408 |