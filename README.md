# ONAP testing

This project aims to execute ONAP community xtesting testsuites on any ONAP
solution.
It can be included in any CI/CD chain or run directly from the ansible playbooks.

We use the xtesting dockers built by the ONAP community. The
xtesting dockers are:

- infra-healthcheck
- healthcheck
- smoke-usecases-robot
- smoke-usecases-pythonsdk
- security

The possible tests per category are are:

- infra-healthcheck
  - onap-k8s
  - onap-helm
  - nodeport-certs
  - internal_certs: check the validity of the internal ONAP ports
- healthcheck
  - core
  - full
  - healthdist
  - postinstall
  - dcaemod
  - hv-ves
  - ves-collector
- smoke-usecases-robot
  - 5gbulkpm
  - pnf-registrate
  - cmpv2
- smoke-usecases-pythonsdk
  - basic_onboard: onboard a model in SDC
  - basic_vm: onboard a model of a VM, distribute then instantiate using
    a_la_carte bpmn
  - basic_vm_macro: basic vm instantiation and onboarding using macro mode
  - basic_network: onboard a model of an Openstack Neutron network, distribute then
    instantiate using a_la_carte bpmn
  - basic_cds: upload and enrich a CBA in CDS
  - basic_clamp: onboard a model, enrich it with a TCA, create a loop with clamp
    then instantiate the loop and distribute it in Policy and DCAE
- security
  - root_pods: check that there is no pods launched as root
  - unlimitted_pods: check that the ONAP pods are launched with limits
  - jdpw_ports: check that no jdpw port is open
  - nonssl_endpoints: check the non ssl exposed ports
  - versions: check the java and python versions

The full documentation can found in ONAP official documentation:
<https://docs.onap.org/projects/onap-integration/en/latest/integration-tests.html>

## inputs

### Configuration files

It is assumed that some files are available on the ansible execution target
environment:

- the ONAP kubernetes credentials (~/.kube/config)
- the Openstack Cloud credentials (~/.config/openstack/clouds.yaml)

These variables may be retrieved from previous projects if using chained-ci.
If not they can be provided manually.

### Environment variables

Several environment can be used by the collection

- mandatory (for chained-CI only):
  - PRIVATE_TOKEN: to get the artifact
  - artifacts_src: the url to get the artifacts
  - OR artifacts_bin: b64_encoded zipped artifacts (tbd)
  - ANSIBLE_VAULT_PASSWORD: the vault password needed by ciphered ansible vars
- optional:
  - ANSIBLE_VERBOSE:
    - role: verbose option for ansible
    - values: "", "-vvv"
    - default: ""
  - ONAP_VERSION:
    - role: the version of xtesting dockers to be used
    - value: "master", "honolulu", "guilin"
    - default: master
  - ONAP_NAMESPACE:
    - role: the namespace deployment in kubernetes
    - value type: string
    - default: "onap"
  - POD:
    - role: name of the pod when we'll insert healtcheck results
    - value type;: string
    - default: empty
  - BUILD_TAG:
    - role: specify a build tag for the results
    - value type;: string
    - default: empty
  - DEPLOYMENT:
    - role: name of the deployment for right tagging when we'll insert
      healtcheck results
    - value type: string
    - default: "rancher"
  - DEPLOYMENT_TYPE:
    - role: type of ONAP deployment expected
    - values: "core", "small", "medium", "full"
    - default: "full"
  - TEST_RESULT_DB_URL:
    - role: url of test db api
    - value type: string
<!-- markdown-link-check-disable -->
    - default: <http://testresults.opnfv.org/test/api/v1/results>
<!-- markdown-link-check-enable -->
  - S3_ENDPOINT_URL:
    - role: S3 endpoint to store results
    - value type: boolean
    - default: true
  - S3_INTERNAL_ENDPOINT_URL:
    - role: S3 internal endpoint to store results
    - value type: boolean
    - default: true
  - S3_HTTP_DST_URL:
    - role: S3 HTTP destination URL
    - value type: boolean
    - default: true
  - TEST_IDENTIFIER:
    - role: random test identifier
    - value type: string
  - S3_ENDPOINT_URL:
    - role: S3 endpoint to store results
    - value type: boolean
    - default: true
  - S3_ACCESS_KEY:
    - role: S3 access key to store results
    - value type: string
    - default: ''
  - S3_SECRET_KEY:
    - role: S3 secret key to store results
    - value type: string
    - default: ''
  - CNF_TEST_K8S_REGION_TYPE:
    - role: K8S Region type for CNF tests
    - value type: string
    - default: 'k8s'
  - CNF_TEST_K8S_CONFIG:
    - role: kube config for CNF tests (by default the ONAP config is reused)
    - value type: string
    - default: 'None'
  - CNF_TEST_K8S_NAMESPACE:
    - role: kube namespace for CNF tests
    - value type: string
    - default: 'onap'

### Ansible variables

Mandatory

- onap_testing_test_name: the name of the test
  it must be an existing test

Optional

- onap_testing_nb_max_retry (docker): number of retries
  in case of failure (default 0)
- onap_testing_run_timeout: the test timeout value
  in minutes (default 30m)
- onap_install_external_requirements: to allow the installation of external
  requirements (set to true by default). A test launch from a gitlab-CI runner
  may not request to install external requirements

## outputs

As the tests are executed within xtesting dockers, they automatically leverage
built-in mechanisms dealing with artifacts and result management.

By default, xtesting will try to publish the results on the DB declared  via
the env variable TEST_RESULT_DB_URL.
Note the env variables POD, DEPLOYMENT, DEPLOYMENT_TYPE, BUILD_TAG may be used
to specify the results.

The artifacts can be pushed to a S3 bucket, but all the S3 env variables shall
be precised. The artifacts of the tests based on k8s job can be saved only
through this S3 mechanism. For docker based test, results can be retrieved on
the /tmp folder of the execution machine in addition of the
S3 bucket, if the S3 env variables are properly set.

## Examples

```bash
ansible-playbook <collection_path>/ansible_collections\
/orange/onap_testing/playbooks/run_onap_test.yml -i
/tmp/vm-test --extra-vars "onap_testing_test_name=onap-helm"

ansible-playbook <collection_path>/ansible_collections\
/orange/onap_testing/playbooks/run_onap_test.yml -i
/tmp/vm-test --extra-vars
"onap_testing_test_name=basic_network onap_testing_run_timeout=30"

ansible-playbook <collection_path>/ansible_collections\
/orange/onap_testing/playbooks/run_onap_test.yml -i
/tmp/vm-test --extra-vars
"onap_testing_test_name=unlimitted_pods
onap_testing_run_timeout=30
onap_testing_nb_max_retry=2"
```
