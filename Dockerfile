FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core:2.11
LABEL maintainer="Morgan Richomme <morgan.richomme@orange.com>"

ARG VCS_REF
ARG BUILD_DATE

ENV APP /opt/onap_testing/
ENV COLLECTION_PATH ~/.ansible/collections

WORKDIR $APP

COPY . $APP/

RUN mkdir -p "$COLLECTION_PATH" && \
    ansible-galaxy collection install "git+file://$PWD/.git" \
      -p "$COLLECTION_PATH" && \
    rm -rf "$APP/.git"
